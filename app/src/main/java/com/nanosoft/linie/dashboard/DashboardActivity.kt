package com.nanosoft.linie.dashboard

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.dashboard.fragment.HomeFragment
import com.nanosoft.linie.dashboard.fragment.OrdersFragment
import com.nanosoft.linie.dashboard.fragment.SettingsFragment
import kotlinx.android.synthetic.main.activity_dashboard.*


/**
 * Created by Vijay on 4/12/19.
 */

@SuppressLint("Registered")
class DashboardActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    companion object {

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        replaceFragment(
            R.id.dashboard_container,
            HomeFragment()
        )

        bottom_nav_dashboard.setOnNavigationItemSelectedListener(this)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_home -> {
                replaceFragment(
                    R.id.dashboard_container,
                    HomeFragment()
                )
                return true
            }
            R.id.menu_settings -> {
                replaceFragment(
                    R.id.dashboard_container,
                    SettingsFragment()
                )
                return true
            }
            R.id.menu_orders -> {
                replaceFragment(
                    R.id.dashboard_container,
                    OrdersFragment()
                )
                return true
            }
            /* R.id.menu_notification -> {
                 replaceFragment(
                     R.id.dashboard_container,
                     NotificationFragment()
                 )
                 return true
             }*/

        }
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}