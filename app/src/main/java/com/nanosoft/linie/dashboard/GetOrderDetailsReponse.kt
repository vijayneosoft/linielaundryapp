package com.nanosoft.linie.dashboard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 19/2/20.
 */
class GetOrderDetailsReponse {
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("orderListData")
    @Expose
    var orderListData: List<OrderListDetails>? = null

    class OrderListDetails{

        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("cloths")
        @Expose
        var cloths: List<ClothDetails>? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("service_id")
        @Expose
        var serviceId: ServiceId? = null
        @SerializedName("user_id")
        @Expose
        var userId: UserId? = null
        @SerializedName("user_type")
        @Expose
        var userType: String? = null
        @SerializedName("payment_mode")
        @Expose
        var paymentMode: String? = null
        @SerializedName("final_cost")
        @Expose
        var finalCost: Int? = null
        @SerializedName("discount")
        @Expose
        var discount: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("pickup_date")
        @Expose
        var pickupDate: String? = null
        @SerializedName("drop_date")
        @Expose
        var dropDate: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }

    class UserId{

        @SerializedName("mobile")
        @Expose
        var mobile: List<String>? = null
        @SerializedName("device_token")
        @Expose
        var deviceToken: List<Any>? = null
        @SerializedName("user_status")
        @Expose
        var userStatus: String? = null
        @SerializedName("isLoggedIn")
        @Expose
        var isLoggedIn: Boolean? = null
        @SerializedName("user_country")
        @Expose
        var userCountry: String? = null
        @SerializedName("permissions")
        @Expose
        var permissions: List<Any>? = null
        @SerializedName("wallet_balance")
        @Expose
        var walletBalance: Int? = null
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("user_type")
        @Expose
        var userType: String? = null
        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("dob")
        @Expose
        var dob: Any? = null
        @SerializedName("user_city")
        @Expose
        var userCity: String? = null
        @SerializedName("user_state")
        @Expose
        var userState: String? = null
        @SerializedName("password")
        @Expose
        var password: String? = null
        @SerializedName("profileImage")
        @Expose
        var profileImage: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
        @SerializedName("otp")
        @Expose
        var otp: Int? = null
        @SerializedName("otp_expired")
        @Expose
        var otpExpired: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_address")
        @Expose
        var userAddress: String? = null
    }

    class ServiceId {
        @SerializedName("isActive")
        @Expose
        var isActive: Boolean? = null
        @SerializedName("isDeleted")
        @Expose
        var isDeleted: Boolean? = null
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("Type")
        @Expose
        var type: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("images")
        @Expose
        var images: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }

    class ClothDetails{
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("quantity")
        @Expose
        var quantity: String? = null
        @SerializedName("selectedCloth")
        @Expose
        var selectedCloth: SelectedClothDetails? = null

    }
    
    class SelectedClothDetails{
        @SerializedName("isActive")
        @Expose
        var isActive: Boolean? = null
        @SerializedName("isDeleted")
        @Expose
        var isDeleted: Boolean? = null
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_Type")
        @Expose
        var userType: String? = null
        @SerializedName("materail_type")
        @Expose
        var materailType: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("cost")
        @Expose
        var cost: String? = null
        @SerializedName("gst")
        @Expose
        var gst: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }
}