package com.nanosoft.linie.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nanosoft.linie.R


/**
 * Created by Vijay on 8/2/20.
 */

class GetOrdersDetailsAdapter(
    var mContext: Context,
    var mListOrders: GetOrderDetailsReponse.OrderListDetails,
    var onItemClick: OnDetailsItem
) : RecyclerView.Adapter<GetOrdersDetailsAdapter.OrderViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.adapter_order_details_new, parent, false)
        return OrderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListOrders.cloths?.size!!
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class OrderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var txtInventoryList: TextView? = null
        var txtInventoryQty: TextView? = null
        var txtInventoryUnitPrice: TextView? = null
        var txtInventoryTotalPrice: TextView? = null
        var txtServicesOrder: TextView? = null


        init {
            txtServicesOrder = view.findViewById<TextView>(R.id.txtServicesOrder)
            txtInventoryList = view.findViewById<TextView>(R.id.txtInventoryList)
            txtInventoryQty = view.findViewById<TextView>(R.id.txtInventoryQty)
            txtInventoryUnitPrice = view.findViewById<TextView>(R.id.txtInventoryUnitPrice)
            txtInventoryTotalPrice = view.findViewById<TextView>(R.id.txtInventoryTotalPrice)

        }

        fun bind(position: Int) {
            val data = mListOrders.cloths?.get(position)?.selectedCloth
            txtInventoryList?.setText("" + data?.name)
            txtServicesOrder?.setText("UserType: " + data?.userType)
            txtInventoryQty?.setText("" + mListOrders.cloths?.get(position)?.quantity)
            txtInventoryUnitPrice?.setText("" + data?.cost)
            txtInventoryTotalPrice?.setText("Rs. " + mListOrders.finalCost)

        }
    }

    private fun onItemClick(adapterPosition: Int) {
//        mList?.get(adapterPosition)?.let { Navigator.navigateToWashActivity(mContext, it.id) }
    }

    interface OnDetailsItem {
        fun onOrdersDetails(get: GetOrderDetailsReponse.OrderListDetails)
    }


}