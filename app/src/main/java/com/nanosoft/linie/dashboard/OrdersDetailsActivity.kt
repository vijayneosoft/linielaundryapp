package com.nanosoft.linie.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_order_details.*

class OrdersDetailsActivity : BaseActivity(), View.OnClickListener,
    GetOrdersDetailsAdapter.OnDetailsItem {

    companion object {
        var KEY_ORDER_DETAILS = "keyOrderDetails"
        fun getCallingIntent(context: Context, mOrderData: String): Intent {
            val intent = Intent(context, OrdersDetailsActivity::class.java)
            intent.putExtra(KEY_ORDER_DETAILS, mOrderData)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        initListeners()
        setToolbarWithTitle("Orders Details")
        showBackArrow()

        val bundle = intent.extras
        if (bundle != null) {
            val value = bundle.getString(KEY_ORDER_DETAILS)
            val orderData = gson.fromJson<GetOrderDetailsReponse.OrderListDetails>(
                value,
                GetOrderDetailsReponse.OrderListDetails::class.java
            )
            setOrdersDetailsAdapter(orderData)
            txtUserName.setText(
                "Username: " + mSharedPreferenceManager?.getString(
                    SharedPreferenceManager.KEY_NAME
                )
            )
        }

    }

    private fun initListeners() {

    }

    override fun onClick(view: View?) {
        when (view?.id) {

        }

    }

    private fun setOrdersDetailsAdapter(mListOrders: GetOrderDetailsReponse.OrderListDetails) {
        try {
            val linearLayoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rvOrderDetails.layoutManager = linearLayoutManager

            val ordersAdapter = GetOrdersDetailsAdapter(this, mListOrders, this)
            rvOrderDetails.adapter = ordersAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onOrdersDetails(get: GetOrderDetailsReponse.OrderListDetails) {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
