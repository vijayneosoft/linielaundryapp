package com.nanosoft.linie.dashboard.fragment

import android.os.Bundle
import android.view.View
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseFragment
import com.nanosoft.linie.dashboard.DashboardActivity

/**
 * Created by Vijay on 5/12/19.
 */

class NotificationFragment : BaseFragment() {

    override fun getLayout(): Int {
        return R.layout.fragment_notification
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).setToolbarWithTitle("Notification")


    }

}