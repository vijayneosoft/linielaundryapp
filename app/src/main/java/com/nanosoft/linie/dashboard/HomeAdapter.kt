package com.nanosoft.linie.dashboard

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nanosoft.linie.R
import com.nanosoft.linie.models.ServicesResponse
import com.nanosoft.linie.utils.Constants
import com.nanosoft.linie.utils.Navigator

/**
 * Created by Vijay on 8/1/20.
 */

class HomeAdapter(
    var mContext: Context,
    var listOfImages: List<Int>,
    var servicesRes: ServicesResponse
) : RecyclerView.Adapter<HomeAdapter.WashViewHolder>() {

    var mList: List<ServicesResponse.ServicesList>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WashViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.adapter_home, parent, false)
        return WashViewHolder(view)
    }

    override fun getItemCount(): Int {
        return servicesRes.servicesList?.size!!
    }

    override fun onBindViewHolder(holder: WashViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class WashViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var countAdd = 0
        var imgServices: ImageView? = null
        var txtServicesName: TextView? = null

        init {
            imgServices = view.findViewById<ImageView>(R.id.imgServices)
            txtServicesName = view.findViewById<TextView>(R.id.txtServicesName)

            view.setOnClickListener({ v -> onItemClick(adapterPosition) })
        }

        fun bind(position: Int) {
            mList = servicesRes.servicesList

            txtServicesName?.setText(mList?.get(position)?.name)

            Glide.with(mContext)
                .load(Constants.BASE_URL_SERVICES_IMAGE + mList?.get(position)?.images)
                .into(imgServices)

            Log.d("wasfdsf", "" + Constants.BASE_URL_SERVICES_IMAGE + mList?.get(position)?.images)

           /* if (servicesRes.servicesList?.size!! <= listOfImages.get(position))
                imgServices?.setImageDrawable(mContext.getDrawable(listOfImages.get(position)))*/

        }

    }

    private fun onItemClick(adapterPosition: Int) {
        mList?.get(adapterPosition)?.let { Navigator.navigateToWashActivity(mContext, it.id) }
    }


}