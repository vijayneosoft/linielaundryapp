package com.nanosoft.linie.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nanosoft.linie.R
import java.text.SimpleDateFormat


/**
 * Created by Vijay on 8/2/20.
 */

class GetOrdersAdapter(
    var mContext: Context,
    var mListOrders: List<GetOrderDetailsReponse.OrderListDetails>?,
    var onItemClick: OnDetailsItem
) : RecyclerView.Adapter<GetOrdersAdapter.OrderViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.adapter_get_orders_new, parent, false)
        return OrderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListOrders?.size!!
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class OrderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var txtQuantity: TextView? = null
        var txtPickup: TextView? = null
        var txtDate: TextView? = null
        var txtCostOrders: TextView? = null
        var txtSerialNumber: TextView? = null
        var txtDetails: TextView? = null


        init {
            txtSerialNumber = view.findViewById<TextView>(R.id.txtSerialNumber)
            txtQuantity = view.findViewById<TextView>(R.id.txtQuantityOrders)
            txtPickup = view.findViewById<TextView>(R.id.txtPickupDateOrders)
            txtDate = view.findViewById<TextView>(R.id.txtDropDateOrders)
            txtCostOrders = view.findViewById<TextView>(R.id.txtCostOrders)

            txtDetails = view.findViewById<TextView>(R.id.txtOrderDetails)
            view.setOnClickListener({ v -> onItemClick(adapterPosition) })

        }

        fun bind(position: Int) {

            val number = position + 1
//            txtSerialNumber?.setText("" + number)
            txtQuantity?.setText("Quantity: " + mListOrders?.get(position)?.cloths?.size)
            txtPickup?.setText("Pickup Date: " + formatDate(mListOrders?.get(position)?.pickupDate))
            txtDate?.setText("Delivery Date: " + formatDate(mListOrders?.get(position)?.dropDate))
            txtCostOrders?.setText("Rs. " + mListOrders?.get(position)?.finalCost)
            txtDetails?.setText("" + mListOrders?.get(position)?.status)
        }
    }

    fun formatDate(dropDate: String?): String? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val date = dateFormat.parse(dropDate)
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        val dateStr = formatter.format(date)
        return dateStr
    }

    private fun onItemClick(adapterPosition: Int) {
        mListOrders?.get(adapterPosition)?.let { onItemClick.onOrdersDetails(it) }
    }

    interface OnDetailsItem {
        fun onOrdersDetails(get: GetOrderDetailsReponse.OrderListDetails)
    }


}