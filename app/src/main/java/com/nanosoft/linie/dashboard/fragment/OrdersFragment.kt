package com.nanosoft.linie.dashboard.fragment

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseFragment
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.dashboard.GetOrderDetailsReponse
import com.nanosoft.linie.dashboard.GetOrdersAdapter
import com.nanosoft.linie.utils.Navigator
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_orders_new.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Vijay on 5/12/19.
 */

class OrdersFragment : BaseFragment(), GetOrdersAdapter.OnDetailsItem,
    RadioGroup.OnCheckedChangeListener {

    var mFlag = true
    var mListPlaced = ArrayList<GetOrderDetailsReponse.OrderListDetails>()
    var mListDelivered = ArrayList<GetOrderDetailsReponse.OrderListDetails>()

    override fun getLayout(): Int {
        return R.layout.fragment_orders_new
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).setToolbarWithTitle("Orders")

        callGetOrdersListingApi()

        rgStatus.setOnCheckedChangeListener(this)
        rdBtnPlaced.isChecked = true

    }

    private fun callGetOrdersListingApi() {
        val userId = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_USER_ID)
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)

        mCustomerService?.getUserOrdersListingApi(token!!, userId!!)
            ?.enqueue(object : Callback<GetOrderDetailsReponse> {
                override fun onFailure(call: Call<GetOrderDetailsReponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    (activity as DashboardActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<GetOrderDetailsReponse>,
                    response: Response<GetOrderDetailsReponse>
                ) {

                    mListPlaced.clear()
                    mListDelivered.clear()
                    if (!response.body()?.error!!) {

                        val ordersData = response.body()?.orderListData

                        for (i in 0..ordersData?.size!! - 1) {
                            if (ordersData.get(i).status.equals("Placed") ||
                                ordersData.get(i).status.equals("Requested")
                            ) {
                                mListPlaced.add(ordersData.get(i))
                            } else {
                                mListDelivered.add(ordersData.get(i))
                            }
                        }
                        if (mFlag) {
                            setOrdersAdapter(mListPlaced)
                        } else {
                            setOrdersAdapter(mListDelivered)
                        }
                    }

                }
            })

    }

    fun setOrdersAdapter(mListOrders: List<GetOrderDetailsReponse.OrderListDetails>?) {
        try {
            val linearLayoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            rvGetOrders.layoutManager = linearLayoutManager

            val ordersAdapter = GetOrdersAdapter(activity!!, mListOrders, this)
            rvGetOrders.adapter = ordersAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnPlaced -> {
                mFlag = true
                callGetOrdersListingApi()
            }
            R.id.rdBtnDelivered -> {
                mFlag = false
                callGetOrdersListingApi()
            }
        }
    }

    override fun onOrdersDetails(get: GetOrderDetailsReponse.OrderListDetails) {
        val data = gson.toJson(get)
        Navigator.navigateToOrdersDetailsActivity(activity!!, data)
    }


}