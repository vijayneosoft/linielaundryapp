package com.nanosoft.linie.dashboard.fragment

import android.os.Bundle
import android.view.View
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseFragment
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.utils.Navigator
import kotlinx.android.synthetic.main.fragment_settings.*

/**
 * Created by Vijay on 5/12/19.
 */

class SettingsFragment : BaseFragment(), View.OnClickListener {


    override fun getLayout(): Int {
        return R.layout.fragment_settings
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        settings_logout.setOnClickListener(this)
        change_password_set.setOnClickListener(this)
        settings_edit_profile.setOnClickListener(this)
        settings_terms.setOnClickListener(this)
        settings_faq.setOnClickListener(this)


        (activity as DashboardActivity).setToolbarWithTitle("Settings")


    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.settings_edit_profile -> {
                Navigator.navigateToSignUpActivity(activity!!, true)
            }

            R.id.settings_logout -> {
                mSharedPreferenceManager?.clearData()
                Navigator.navigateToSplashActivity(activity!!)
                activity?.finish()
                activity?.finishAffinity()
            }
            R.id.change_password_set -> {
                Navigator.navigateToForgotActivity(activity as DashboardActivity, true)
            }
            R.id.settings_terms -> {
                Navigator.navigateToOpenActivity(activity!!, false)
            }
            R.id.settings_faq -> {
                Navigator.navigateToOpenActivity(activity!!, true)
            }
        }
    }

}