package com.nanosoft.linie.dashboard.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseFragment
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.dashboard.HomeAdapter
import com.nanosoft.linie.models.ServicesResponse
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Vijay on 5/12/19.
 */

class HomeFragment : BaseFragment() {

    var listOfImages = listOf<Int>(R.drawable.wash, R.drawable.dry, R.drawable.iron)

    override fun getLayout(): Int {
        return R.layout.fragment_home
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            (it as DashboardActivity).showProgress(it)
        }
        callServicesListingApi()
        (activity as DashboardActivity).setToolbarWithTitle("Home")
    }

    private fun callServicesListingApi() {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        mCustomerService?.servicesListingApi(token!!)
            ?.enqueue(object : Callback<ServicesResponse> {
                override fun onResponse(
                    call: Call<ServicesResponse>,
                    response: Response<ServicesResponse>
                ) {
                    if (!response.body()?.error!!) {

                        if (activity is DashboardActivity)
                            activity?.let {
                                (activity as DashboardActivity).hideProgress()
                            }
                        response.body()?.let {
                            setAdapter(it)
                        }
                    } else {
                        showToastMessage("" + response.body()!!.title)
                        if (activity is DashboardActivity)
                            activity.let {
                                (activity as DashboardActivity).hideProgress()
                            }
                    }
                }

                override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    activity?.let {
                        (activity as DashboardActivity).hideProgress()
                    }
                }
            })
    }

    private fun setAdapter(servicesList: ServicesResponse) {
        try {
            val linearLayoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            rvItemsServices.layoutManager = linearLayoutManager
            rvItemsServices.itemAnimator = DefaultItemAnimator()
            activity?.let {
                val homeAdapter = HomeAdapter(it, listOfImages, servicesList)
                rvItemsServices.adapter = homeAdapter
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}