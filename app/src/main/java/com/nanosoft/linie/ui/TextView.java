package com.nanosoft.linie.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;

import com.nanosoft.linie.R;
import com.nanosoft.linie.utils.TypefaceUtil;


public class TextView extends androidx.appcompat.widget.AppCompatTextView {

    private float mLetterSpacing = LetterSpacing.NORMAL;
    private CharSequence mOriginalText = "";

    public TextView(Context context) {
        super(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        parseAttributes(context, attrs);
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        if (isInEditMode()) return;
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.TextView);
        if (values == null) return;

        //The value 0 is a default, but shouldn't ever be used since the attr is an enum
        int typeface = values.getInt(R.styleable.TextView_typeface, 0);
        String link = values.getString(R.styleable.TextView_setLink);
        float letterSpacing = values.getFloat(R.styleable.TextView_letterSpacing, LetterSpacing.NORMAL);


        values.recycle();
        setTypeface(TypefaceUtil.getTypeface(typeface));
        setCustomLetterSpacing(letterSpacing);
    }

    public float getCustomLetterSpacing() {
        return mLetterSpacing;
    }

    public void setCustomLetterSpacing(float letterSpacing) {
        mLetterSpacing = letterSpacing;
        applyLetterSpacing();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        mOriginalText = text;
        applyLetterSpacing();
    }

    @Override
    public CharSequence getText() {
        return super.getText();
    }

    private void applyLetterSpacing() {
        if (mLetterSpacing != LetterSpacing.NORMAL && !TextUtils.isEmpty(mOriginalText)) {
            super.setText(applyKerning(mOriginalText, (mLetterSpacing+1)/10), BufferType.SPANNABLE);
        }
    }

    private static Spannable applyKerning(CharSequence src, float kerning) {
        if (src == null) return null;
        final int srcLength = src.length();
        if (srcLength < 2) return src instanceof Spannable
                ? (Spannable)src
                : new SpannableString(src);

        final String nonBreakingSpace = "\u00A0";
        final SpannableStringBuilder builder = src instanceof SpannableStringBuilder
                ? (SpannableStringBuilder)src
                : new SpannableStringBuilder(src);
        for (int i = src.length()-1; i >= 1; i--) {
            if (src.charAt(i-1) == '\n' || (i < srcLength && src.charAt(i) == '\n')) continue;
            builder.insert(i, nonBreakingSpace);
            builder.setSpan(new ScaleXSpan(kerning), i, i + 1,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return builder;
    }

    public class LetterSpacing {
        public final static float NORMAL = 0;
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (isInEditMode()){
            return;
        }

        // Add some extra width to the text view if using italic font to avoid clipping
        // See: http://stackoverflow.com/questions/4353836/italic-textview-with-wrap-contents-seems-to-clip-the-text-at-right-edge
        if (TypefaceUtil.isItalicTypeface(getTypeface())) {
            final int measuredWidth = getMeasuredWidth();
            // we're aiming for a little less than half the width of a character in this text
            final float extraWidth = (measuredWidth / getText().length()) * 0.4f;
            final int newWidth = measuredWidth + (int) extraWidth;
            setMeasuredDimension(newWidth, getMeasuredHeight());
        }
    }


}
