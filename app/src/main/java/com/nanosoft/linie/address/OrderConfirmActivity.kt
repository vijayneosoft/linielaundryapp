package com.nanosoft.linie.address

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.models.*
import com.nanosoft.linie.services.AddOrderRequest
import com.nanosoft.linie.utils.Navigator
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_order_confirm.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Vijay on 26/1/20.
 */

class OrderConfirmActivity : BaseActivity(), View.OnClickListener,
    DatePickerDialog.OnDateSetListener {

    var isPickupDate = false
    var orderData = AddOrderRequest()
    var edtPickupGMT = ""
    var edtDropDateGMT = ""
    var mAddressSaved = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_confirm)

        initListeners()
        setToolbarWithTitle("Order Confirmation")
        showBackArrow()

        val bundle = intent.extras
        if (bundle != null) {
            val value = bundle.getString(KEY_ORDER_REQUEST)
            orderData = gson.fromJson<AddOrderRequest>(value, AddOrderRequest::class.java)
        }

        txtBasketTotal_comfirm.setText("Rs " + orderData.final_cost)

    }

    companion object {
        var KEY_ORDER_REQUEST = "keyOrderData"
        fun getCallingIntent(context: Context, mOrderData: String): Intent {
            val intent = Intent(context, OrderConfirmActivity::class.java)
            intent.putExtra(KEY_ORDER_REQUEST, mOrderData)
            return intent
        }
    }

    private fun initListeners() {
        txtPickup.setOnClickListener(this)
        txtDeliveryDate.setOnClickListener(this)
        btnOrderConfirm.setOnClickListener(this)
        btnSaveAddress.setOnClickListener(this)
        imgSavedAddress.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.imgSavedAddress -> {
                callGetAddressApi()
            }
            R.id.txtDeliveryDate -> {
                isPickupDate = false
                showDateDialog()
            }
            R.id.txtPickup -> {
                isPickupDate = true
                showDateDialog()
            }
            R.id.btnOrderConfirm -> {
                if (mAddressSaved) {
                    orderData.user_id =
                        mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_USER_ID)!!
                    orderData.pickup_date = edtPickupGMT
                    orderData.drop_date = edtDropDateGMT
                    if (!TextUtils.isEmpty(edtPickupGMT) && !TextUtils.isEmpty(edtDropDateGMT)) {
                        callAddOrderApi(orderData)
                    } else {
                        showToastMessage("Please select Pickup date or Delivery date")
                    }
                } else {
                    showToastMessage("Please enter address")
                }
            }
            R.id.btnSaveAddress -> {

                val city = edtCity_add.text.toString()
                val location = edtLocation_add.text.toString()

                val mobileNo = edtMobileNo_add.text.toString()

                if (!TextUtils.isEmpty(city)
                    && !TextUtils.isEmpty(location) && !TextUtils.isEmpty(mobileNo)
                ) {
                    val addressReq =
                        AddAddressRequest()
                    addressReq.user_id =
                        mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_USER_ID)!!

                    val addressBody = DeliveryAddress()
                    addressBody.city = city
                    addressBody.street =
                        location
                    addressBody.country = "India"
                    addressBody.state = "Maharashtra"
                    addressBody.mobile_no = mobileNo
                    addressBody.isDefault = true
                    addressBody.isPrimary = true

                    addressReq.deliveryAddress = addressBody
                    callAddAddressApi(addressReq)

                } else {
                    showToastMessage(resources.getString(R.string.error_empty_field))
                }


            }
        }
    }

    private fun showDateDialog() {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat("dd MM yyyy")
        val formattedDate = df.format(date)

        val numbers =
            formattedDate.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val dialog = DatePickerDialog(
            this, this, numbers[2].toInt(),
            numbers[1].toInt() - 1, numbers[0].toInt()
        )

        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000)
        dialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, moy: Int, dayOfMonth: Int) {

        val monthOfYear = moy + 1;
        val string = "" + year + "-" + monthOfYear + "-" + dayOfMonth
        val format1 = SimpleDateFormat("yyyy-MM-dd")
        format1.timeZone = TimeZone.getTimeZone("GMT")

        var date: Date? = null
        try {
            date = format1.parse(string) //GMT format
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        if (isPickupDate) {
            edtPickupGMT = "" + date
            txtPickup.setText("" + dayOfMonth + " / " + monthOfYear + " / " + year)
        } else {
            edtDropDateGMT = "" + date
            txtDeliveryDate.setText("" + dayOfMonth + " / " + monthOfYear + " / " + year)
        }

    }

    fun callAddOrderApi(mOrderRequest: AddOrderRequest) {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        mCustomerService?.addOrderApi(token!!, mOrderRequest)
            ?.enqueue(object : Callback<AddOrderResponse> {
                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {

                    if (!response.body()?.error!!) {
                        hideProgress()
                        showToastMessage("Order Confirmed")

                        Navigator.navigateToDashboardActivity(this@OrderConfirmActivity)

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun callAddAddressApi(addressReq: AddAddressRequest) {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        mCustomerService?.addDeliveryAddressApi(token!!, addressReq)
            ?.enqueue(object : Callback<AddAddressResponse> {
                override fun onResponse(
                    call: Call<AddAddressResponse>,
                    response: Response<AddAddressResponse>
                ) {
                    if (!response.body()?.error!!) {
                        showToastMessage("" + response.body()!!.title)
                        mAddressSaved = true
                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<AddAddressResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                }
            })
    }

    private fun callGetAddressApi() {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        val userId = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_USER_ID)
        mCustomerService?.getDeliveryAddressDataApi(token!!, userId!!)
            ?.enqueue(object : Callback<GetAddressResponse> {
                override fun onResponse(
                    call: Call<GetAddressResponse>,
                    response: Response<GetAddressResponse>
                ) {
                    if (!response.body()?.error!!) {
                        val data = response.body()?.userdata?.deliveryAddress
                        showToastMessage("" + response.body()!!.title)
                        edtLocation_add.setText("" + data?.get(data.lastIndex)?.street)
                        edtCity_add.setText("" + data?.get(data.lastIndex)?.city)
                        edtMobileNo_add.setText("" + data?.get(data.lastIndex)?.mobileNo)
                        mAddressSaved = true

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<GetAddressResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                }
            })
    }


}
