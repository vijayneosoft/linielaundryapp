package com.nanosoft.linie.messageService

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.nanosoft.linie.R
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.utils.SharedPreferenceManager


/**
 * Created by Vijay on 26/1/20.
 */

class LaundryMessagingService : FirebaseMessagingService() {

    var preferenceManager = SharedPreferenceManager()
    var KEY_DEVICE_TOKEN = "deviceToken"
    var KEY_CONTENT = "body"
    var KEY_TITLE = "title"


    override fun onNewToken(token: String) {
        super.onNewToken(token)
        preferenceManager.saveString(KEY_DEVICE_TOKEN, token)
        Log.d("token", "" + token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val message = remoteMessage.data

        Log.d(TAG, "Message" + remoteMessage.data)

        remoteMessage.data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }

        val intent = Intent(applicationContext, DashboardActivity::class.java)
        val pi = PendingIntent.getActivity(getApplicationContext(), 101, intent, 0);
        val nm =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channel: NotificationChannel;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel =
                NotificationChannel("222", "my_channel", NotificationManager.IMPORTANCE_HIGH);
            nm.createNotificationChannel(channel);
        }

        val builder =
            NotificationCompat.Builder(
                getApplicationContext(), "222"
            )
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        getResources(),
                        R.mipmap.ic_launcher
                    )
                )
                .setContentTitle(message.get(KEY_TITLE))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                .setContentText(message.get(KEY_CONTENT))
                .setContentIntent(pi)

        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        nm.notify(101, builder.build())
    }

}

