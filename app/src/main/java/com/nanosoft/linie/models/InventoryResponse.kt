package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 12/1/20.
 */

class InventoryResponse {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("menData")
    @Expose
    var menData: List<MenDatum>? = null
    @SerializedName("womenData")
    @Expose
    var womenData: List<WomenDatum>? = null
    @SerializedName("girlData")
    @Expose
    var girlData: List<GirlDatum>? = null
    @SerializedName("boyData")
    @Expose
    var boyData: List<BoyDatum>? = null

    inner class MenDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("materail_type")
        @Expose
        var materailType: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("cost")
        @Expose
        var cost: String? = null
    }

    class WomenDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_Type")
        @Expose
        var userType: String? = null
        @SerializedName("category")
        @Expose
        var category: String? = null
        @SerializedName("materail_type")
        @Expose
        var materailType: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("cost")
        @Expose
        var cost: String? = null
        @SerializedName("gst")
        @Expose
        var gst: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }

    class BoyDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_Type")
        @Expose
        var userType: String? = null
        @SerializedName("category")
        @Expose
        var category: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("materail_type")
        @Expose
        var materailType: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("cost")
        @Expose
        var cost: String? = null
        @SerializedName("gst")
        @Expose
        var gst: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }

    class GirlDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_Type")
        @Expose
        var userType: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("category")
        @Expose
        var category: String? = null
        @SerializedName("materail_type")
        @Expose
        var materailType: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("cost")
        @Expose
        var cost: String? = null
        @SerializedName("gst")
        @Expose
        var gst: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }


}