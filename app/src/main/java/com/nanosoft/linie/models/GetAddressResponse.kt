package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 2/2/20.
 */

class GetAddressResponse {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("userdata")
    @Expose
    var userdata: UserdataDelivery? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null

    class UserdataDelivery {
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("deliveryAddress")
        @Expose
        var deliveryAddress: List<DeliveryAddress>? = null
        @SerializedName("user_id")
        @Expose
        var userId: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
    }

    class DeliveryAddress {
        @SerializedName("isPrimary")
        @Expose
        var isPrimary: Boolean? = null
        @SerializedName("isDefault")
        @Expose
        var isDefault: Boolean? = null
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("mobile_no")
        @Expose
        var mobileNo: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("street")
        @Expose
        var street: String? = null
    }
}