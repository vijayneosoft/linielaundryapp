package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 2/2/20.
 */

class EditResponse {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("user")
    @Expose
    var user: User? = null

    class User {
        @SerializedName("mobile")
        @Expose
        var mobile: List<String>? = null
        @SerializedName("device_token")
        @Expose
        var deviceToken: List<Any>? = null
        @SerializedName("user_status")
        @Expose
        var userStatus: String? = null
        @SerializedName("isLoggedIn")
        @Expose
        var isLoggedIn: Boolean? = null
        @SerializedName("user_country")
        @Expose
        var userCountry: String? = null
        @SerializedName("permissions")
        @Expose
        var permissions: List<Any>? = null
        @SerializedName("wallet_balance")
        @Expose
        var walletBalance: Int? = null
        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("user_type")
        @Expose
        var userType: String? = null
        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("dob")
        @Expose
        var dob: Any? = null
        @SerializedName("user_city")
        @Expose
        var userCity: String? = null
        @SerializedName("user_state")
        @Expose
        var userState: String? = null
        @SerializedName("password")
        @Expose
        var password: String? = null
        @SerializedName("profileImage")
        @Expose
        var profileImage: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null
        @SerializedName("otp")
        @Expose
        var otp: Int? = null
        @SerializedName("otp_expired")
        @Expose
        var otpExpired: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("user_address")
        @Expose
        var userAddress: String? = null
    }
}