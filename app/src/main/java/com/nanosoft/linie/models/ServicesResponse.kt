package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 3/12/19.
 */

class ServicesResponse {


    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("servicesList")
    @Expose
    var servicesList: List<ServicesList>? = null

    class ServicesList {

        @SerializedName("_id")
        @Expose
        var id: String? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("isActive")
        @Expose
        var isActive: Boolean? = null
        @SerializedName("isDeleted")
        @Expose
        var isDeleted: Boolean? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("Type")
        @Expose
        var type: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("images")
        @Expose
        var images: String? = null
        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
        @SerializedName("updatedAt")
        @Expose
        var updatedAt: String? = null
        @SerializedName("__v")
        @Expose
        var v: Int? = null


    }

}