package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 29/12/19.
 */

class ForgotPasswordResponse {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("detail")
    @Expose
    var detail: DetailForgot? = null

    class DetailForgot {

        @SerializedName("code")
        @Expose
        var code: String? = null
        @SerializedName("response")
        @Expose
        var response: String? = null
        @SerializedName("responseCode")
        @Expose
        var responseCode: Int? = null
        @SerializedName("command")
        @Expose
        var command: String? = null

    }

}