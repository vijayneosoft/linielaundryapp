package com.nanosoft.linie.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by Vijay on 12/1/20.
 */

class AddOrderResponse {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("error")
    @Expose
    var error: Boolean? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("service_id")
    @Expose
    var serviceId: String? = null
    @SerializedName("user_id")
    @Expose
    var userId: String? = null
    @SerializedName("payment_mode")
    @Expose
    var paymentMode: String? = null
    @SerializedName("user_type")
    @Expose
    var userType: String? = null
    @SerializedName("cloths")
    @Expose
    var cloths: List<ClothOrder>? = null
    @SerializedName("final_cost")
    @Expose
    var finalCost: Int? = null
    @SerializedName("discount")
    @Expose
    var discount: Int? = null
    @SerializedName("pickup_date")
    @Expose
    var pickupDate: String? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("promo_code")
    @Expose
    var promoCode: String? = null

    inner class ClothOrder {

        @SerializedName("quantity")
        @Expose
        var quantity: String? = null
        @SerializedName("selectedCloth")
        @Expose
        var selectedCloth: String? = null
    }


}