package com.nanosoft.linie.models

/**
 * Created by Vijay on 1/2/20.
 */

class AddAddressRequest {

    var user_id = ""
    var deliveryAddress: DeliveryAddress =
        DeliveryAddress()

}

class DeliveryAddress {
    var street = ""
    var city = ""
    var state = ""
    var country = ""
    var mobile_no = ""
    var isPrimary = false
    var isDefault = false
}
