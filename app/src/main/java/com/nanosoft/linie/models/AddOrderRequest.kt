package com.nanosoft.linie.services


/**
 * Created by Vijay on 18/1/20.
 */

class AddOrderRequest {

    var name: String = ""
    var service_id: String = ""
    var user_id: String = ""
    var user_type: String = ""
    var final_cost: Int = 0
    var discount: Int = 0
    var pickup_date: String = ""
    var drop_date: String = ""

    var payment_mode: String = ""
    var status: String = ""
    var promo_code: String = ""
    var cloths: ArrayList<ClothRequest> = ArrayList()


}

class ClothRequest {
    var quantity: String = ""
    var selectedCloth: String = ""

    override fun toString(): String {
        return "ClothRequest(quantity=$quantity, selectedCloth=$selectedCloth)"
    }

}
