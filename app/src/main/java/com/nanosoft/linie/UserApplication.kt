package com.nanosoft.linie

import android.app.Application
import android.util.Log
import com.facebook.stetho.Stetho
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.nanosoft.linie.utils.SharedPreferenceManager


/**
 * Created by Vijay on 26/2/19.
 */

@Suppress("DEPRECATION")
class UserApplication : Application() {

    companion object {

        var mAppContext: UserApplication? = null

        fun getAppInstance(): UserApplication {
            return mAppContext!!
        }
    }

    override fun onCreate() {
        super.onCreate()
        mAppContext = this
        Stetho.initializeWithDefaults(this)

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.d("Token", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                val mSharedPreferenceManager = SharedPreferenceManager()
                mSharedPreferenceManager.saveString(
                    SharedPreferenceManager.KEY_DEVICE_TOKEN,
                    token!!
                )
                // Log and toast
                Log.d("Token111", token)
            })
    }


}