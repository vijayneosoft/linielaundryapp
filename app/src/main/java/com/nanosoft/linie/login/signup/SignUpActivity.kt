package com.nanosoft.linie.login.signup

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.DatePicker
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.models.EditResponse
import com.nanosoft.linie.models.LoginResponse
import com.nanosoft.linie.utils.SharedPreferenceManager
import com.nanosoft.linie.utils.validateTextIsNotEmpty
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Vijay on 5/12/19.
 */

@SuppressLint("Registered")
class SignUpActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener,
    View.OnClickListener, DatePickerDialog.OnDateSetListener {

    var mGenderSelected = ""
    var edtEmail: String = ""
    var edtDob: String = ""
    var edtMobileNumber: String = ""
    var edtName: String = ""
    var edtCity: String = ""
    var edtPassword: String = ""
    var edtUserType: String = ""
    var edtUserState: String = ""
    var rdGender: String = ""
    var fromEditProfile: Boolean = false

    companion object {

        var KEY_FROM_SCREEN = "keyFromEditProfile"

        fun getCallingIntent(context: Context, fromScreen: Boolean): Intent {
            var intent = Intent(context, SignUpActivity::class.java)
            intent.putExtra(KEY_FROM_SCREEN, fromScreen)
            return intent
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        mGenderSelected = "male"

        val bundle = intent.extras

        if (bundle != null) {
            fromEditProfile = bundle.getBoolean(KEY_FROM_SCREEN)
        }

        initListeners()

        if (fromEditProfile) {
            setToolbarWithTitle("Edit Profile")
            showBackArrow()
            btnConfirm_sp.visibility = View.VISIBLE
            btnRegister_ud.visibility = View.GONE
            txtDetails.visibility = View.GONE

            edt_name_user_details.setText(
                "" + mSharedPreferenceManager?.getString(
                    SharedPreferenceManager.KEY_NAME
                )
            )
            edt_city_ud.setText("" + mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_DEVICE_CITY))
            edt_whatsappNo_ud.setText(
                "" + mSharedPreferenceManager?.getString(
                    SharedPreferenceManager.KEY_DEVICE_MOBILE
                )
            )
            edt_email_ud.setText("" + mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_DEVICE_EMAIL))
            val gender =
                mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_DEVICE_GENDER)

            if (gender.equals("male", false)) {
                rd_btn_male_ud.isChecked = true
            } else {
                rd_btn_female_ud.isChecked = true
            }

            val date1 = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_DEVICE_DOB)

            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            val outputFormat = SimpleDateFormat("dd-MM-yyyy");
            val date = inputFormat.parse(date1);
            val formattedDate = outputFormat.format(date)

            edt_dob_ud.setText("" + formattedDate)
            edt_password_ud.isEnabled = false

        } else {
            setToolbarWithTitle("Sign Up")
            btnRegister_ud.visibility = View.VISIBLE
            btnConfirm_sp.visibility = View.GONE
        }


    }

    private fun initListeners() {
        btnRegister_ud.setOnClickListener(this)
        rd_btn_male_ud.setOnCheckedChangeListener(this)
        rd_btn_female_ud.setOnCheckedChangeListener(this)
        edt_dob_ud.setOnClickListener(this)
        btnConfirm_sp.setOnClickListener(this)

    }

    override fun onCheckedChanged(view: CompoundButton?, p1: Boolean) {

        when (view?.id) {
            R.id.rd_btn_male_ud ->
                mGenderSelected = "male"
            R.id.rd_btn_female_ud ->
                mGenderSelected = "female"
        }

    }

    override fun onClick(view: View?) {

        when (view?.id) {
            R.id.btnRegister_ud -> {
                edtName = edt_name_user_details.text.toString()
                edtEmail = edt_email_ud.text.toString()
                edtMobileNumber = edt_whatsappNo_ud.text.toString()
                edtCity = edt_city_ud.text.toString()
                edtPassword = edt_password_ud.text.toString()
                edtUserType = "buyer"
                edtUserState = "Maharashtra"
                rdGender = mGenderSelected

                if (validateTextIsNotEmpty(edtName) && validateTextIsNotEmpty(edtEmail) && validateTextIsNotEmpty(
                        edtDob
                    ) && validateTextIsNotEmpty(
                        edtMobileNumber
                    ) && validateTextIsNotEmpty(
                        edtCity
                    )
                    && validateTextIsNotEmpty(edtPassword) && validateTextIsNotEmpty(rdGender)
                ) {

                    if (isNetConnected(this)) {
                        showProgress(this)
                        callSignUpApi()
                    } else {
                        showToastMessage(getString(R.string.turn_on_internet))
                    }

                } else {
                    showToastMessage("All the fields are mandatory")
                }
            }

            R.id.edt_dob_ud -> {
                val date = Calendar.getInstance().time
                val df = SimpleDateFormat("dd MM yyyy")
                val formattedDate = df.format(date)

                val numbers =
                    formattedDate.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                val dialog = DatePickerDialog(
                    this, this, numbers[2].toInt(),
                    numbers[1].toInt() - 1, numbers[0].toInt()
                )
                dialog.show()
            }

            R.id.btnConfirm_sp -> {
                callEditProfileApi()
            }
        }

    }


    private fun callSignUpApi() {

        mCustomerService?.doSignUp(
            edtName,
            edtUserType,
            edtEmail,
            rdGender,
            edtDob,
            edtMobileNumber,
            edtCity,
            edtUserState,
            edtPassword
        )
            ?.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    hideProgress()
                    if (!response.body()?.error!!) {
                        showToastMessage("Success")
                        finish()
                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    hideProgress()
                    showToastMessage(getString(R.string.error_something_went_wrong))
                }
            })

    }

    private fun callEditProfileApi() {

        val name = edt_name_user_details.text.toString()
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)

        edtMobileNumber = edt_whatsappNo_ud.text.toString()
        edtCity = edt_city_ud.text.toString()

        rdGender = mGenderSelected

        mCustomerService?.editUserApi(
            token!!,
            "",
            name,
            edtMobileNumber,
            edtDob,
            rdGender,
            edtCity,
            "",
            "Maharashtra",
            "India"
        )
            ?.enqueue(object : Callback<EditResponse> {
                override fun onResponse(
                    call: Call<EditResponse>,
                    response: Response<EditResponse>
                ) {
                    hideProgress()
                    if (!response.body()?.error!!) {
                        showToastMessage("Success")
                        finish()
                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<EditResponse>, t: Throwable) {
                    hideProgress()
                    showToastMessage(getString(R.string.error_something_went_wrong))
                }
            })

    }


    override fun onDateSet(view: DatePicker?, year: Int, moy: Int, dayOfMonth: Int) {

        val monthOfYear = moy + 1;
        val string = "" + year + "-" + monthOfYear + "-" + dayOfMonth
        val format1 = SimpleDateFormat("yyyy-MM-dd")
        format1.timeZone = TimeZone.getTimeZone("GMT")
        var date: Date? = null
        try {
            date = format1.parse(string)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        edtDob = "" + date
        edt_dob_ud.setText("" + dayOfMonth + " / " + monthOfYear + " / " + year)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}