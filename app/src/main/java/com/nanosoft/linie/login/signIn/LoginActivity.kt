package com.nanosoft.linie.login.signIn

import android.os.Bundle
import android.view.View
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.login.signup.SignUpActivity
import com.nanosoft.linie.models.LoginResponse
import com.nanosoft.linie.utils.Navigator
import com.nanosoft.linie.utils.SharedPreferenceManager
import com.nanosoft.linie.utils.validateTextIsNotEmpty
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity(), View.OnClickListener {

    var edtEmail = ""
    var edtPassword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListeners()
    }

    private fun initListeners() {
        btnSignIn_login.setOnClickListener(this)
        llRegister.setOnClickListener(this)
        txtForgotPassword.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnSignIn_login -> {
                edtEmail = edtEmail_login.text.toString()
                edtPassword = edtPassword_login.text.toString()

                if (validateTextIsNotEmpty(edtEmail) && validateTextIsNotEmpty(edtPassword)) {
                    showProgress(this)
                    if (isNetConnected(this)) {
                        callLoginApi()
                    } else {
                        hideProgress()
                        showToastMessage(getString(R.string.turn_on_internet))
                    }
                } else {
                    showToastMessage("Field cannot be empty")
                }
            }
            R.id.llRegister -> {
                openNewActivity(this, SignUpActivity::class.java)
            }
            R.id.txtForgotPassword -> {
                Navigator.navigateToForgotActivity(this, false)
            }
        }

    }

    private fun callLoginApi() {
        val arr = ArrayList<String>()
        arr.clear()
        val deviceToken =
            mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_DEVICE_TOKEN)
        arr.add(deviceToken!!)

        if (deviceToken.isNotEmpty()) {
            mCustomerService?.doLogin(arr, edtEmail, edtPassword, "buyer")
                ?.enqueue(object : Callback<LoginResponse> {
                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {

                        if (!response.body()?.error!!) {
                            hideProgress()
                            showToastMessage("Success")

                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_USER_ID,
                                response.body()!!.userdata?.id!!
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_TOKEN,
                                response.body()!!.token!!
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_NAME,
                                response.body()!!.userdata?.name!!
                            )
                            val mobile = response.body()!!.userdata?.mobile
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_DEVICE_MOBILE,
                                mobile?.get(mobile.lastIndex).toString()
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_DEVICE_EMAIL,
                                response.body()!!.userdata?.email!!
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_DEVICE_GENDER,
                                response.body()!!.userdata?.gender!!
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_DEVICE_CITY,
                                response.body()!!.userdata?.userCity!!
                            )
                            mSharedPreferenceManager?.saveString(
                                SharedPreferenceManager.KEY_DEVICE_DOB,
                                response.body()!!.userdata?.dob!!
                            )

                            mSharedPreferenceManager?.saveBoolean(
                                SharedPreferenceManager.KEY_USER_REGISTERED,
                                true
                            )

                            openNewActivity(this@LoginActivity, DashboardActivity::class.java)
                            finish()
                        } else {
                            showToastMessage("" + response.body()!!.title)
                            hideProgress()
                        }
                    }

                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        showToastMessage(getString(R.string.error_something_went_wrong))
                        hideProgress()

                    }
                })
        } else {
            showToastMessage("Something went wrong! Try again after some time")
            hideProgress()
        }
    }

}
