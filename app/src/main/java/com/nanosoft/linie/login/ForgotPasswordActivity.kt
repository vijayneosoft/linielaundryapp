package com.nanosoft.linie.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.models.ForgotPasswordResponse
import com.nanosoft.linie.utils.SharedPreferenceManager
import com.nanosoft.linie.utils.validateTextIsNotEmpty
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Vijay on 28/12/19.
 */

@SuppressLint("Registered")
class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {

    var fromChangePasswordScreen = false

    companion object {

        var KEY_FROM_SCREEN = "keyFromScreen"

        fun getCallingIntent(context: Context, fromScreen: Boolean): Intent {
            var intent = Intent(context, ForgotPasswordActivity::class.java)
            intent.putExtra(KEY_FROM_SCREEN, fromScreen)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        var bundle = intent.extras

        if (bundle != null) {
            fromChangePasswordScreen = bundle.getBoolean(KEY_FROM_SCREEN)
        }

        initListeners()

        if (fromChangePasswordScreen) {
            setToolbarWithTitle("Change Password")
            llChangePassword.visibility = View.VISIBLE
            llForgotPassword.visibility = View.GONE
        } else {
            setToolbarWithTitle("Forgot Password")
        }
    }

    private fun initListeners() {
        btnForgot_fp.setOnClickListener(this)
        btnVerifyOtp_fp.setOnClickListener(this)
        btnNewPassword_fp.setOnClickListener(this)
        btnChangePassword_fp.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnForgot_fp -> {
//                val btnText = btnForgot_fp.text.toString()
                val edtEmailForgot = edtEmail_fp.text.toString()

                if (validateTextIsNotEmpty(edtEmailForgot)) {
                    callForgotPasswordApi(edtEmailForgot)

                } else {
                    showToastMessage(getString(R.string.error_empty_field))
                }

            }
            R.id.btnVerifyOtp_fp -> {
                val edtVerifyOtp = edtVerifyOtp_fp.text.toString()
                if (validateTextIsNotEmpty(edtVerifyOtp)) {

                    callVerifyOtpApi(edtVerifyOtp)
                } else {
                    showToastMessage(getString(R.string.error_empty_field))
                }

            }
            R.id.btnNewPassword_fp -> {
                val edtNewPassword = edtNewPassword_fp.text.toString()
                if (validateTextIsNotEmpty(edtNewPassword)) {
                    callNewPasswordApi(edtNewPassword)
                } else {
                    showToastMessage(getString(R.string.error_empty_field))
                }

            }
            R.id.btnChangePassword_fp -> {
                val edtOldChange = edtOldChangePassword_fp.text.toString()
                val edtNewChange = edtNewChangePassword_fp.text.toString()
                if (validateTextIsNotEmpty(edtOldChange) && validateTextIsNotEmpty(edtNewChange)) {

                    callChangePasswordApi(edtOldChange, edtNewChange)

                } else {
                    showToastMessage(getString(R.string.error_empty_field))
                }

            }
        }
    }

    private fun callForgotPasswordApi(edtEmailForgot: String) {

        var token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)

        mCustomerService?.forgotPassword(token!!, edtEmailForgot)
            ?.enqueue(object : Callback<ForgotPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgotPasswordResponse>,
                    response: Response<ForgotPasswordResponse>
                ) {
                    hideProgress()

                    if (!response.body()?.error!!) {
                        showToastMessage("Success")

                        setToolbarWithTitle("Verify Otp")
                        llForgotPassword.visibility = View.GONE
                        llVerifyOtp.visibility = View.VISIBLE
                        llNewPassword.visibility = View.GONE

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }

    private fun callVerifyOtpApi(edtVerify: String) {

        mCustomerService?.verifyOtpApi(edtVerify)
            ?.enqueue(object : Callback<ForgotPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgotPasswordResponse>,
                    response: Response<ForgotPasswordResponse>
                ) {
                    hideProgress()

                    if (!response.body()?.error!!) {
                        showToastMessage("Success")
                        setToolbarWithTitle("New Password")
                        llVerifyOtp.visibility = View.GONE
                        llForgotPassword.visibility = View.GONE
                        llNewPassword.visibility = View.VISIBLE

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }

    private fun callChangePasswordApi(edtOldChange: String, edtNewChange: String) {

        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)

        mCustomerService?.changePasswordApi(token!!, edtOldChange, edtNewChange, "changepassword")
            ?.enqueue(object : Callback<ForgotPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgotPasswordResponse>,
                    response: Response<ForgotPasswordResponse>
                ) {
                    hideProgress()
                    if (!response.body()?.error!!) {
                        llVerifyOtp.visibility = View.GONE
                        llForgotPassword.visibility = View.GONE
                        llNewPassword.visibility = View.GONE
                        showToastMessage("Success")
                        finish()

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }

    private fun callNewPasswordApi(edtNewChange: String) {

        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)

        mCustomerService?.newPasswordApi(token!!, edtNewChange, "forgotpassword")
            ?.enqueue(object : Callback<ForgotPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgotPasswordResponse>,
                    response: Response<ForgotPasswordResponse>
                ) {
                    hideProgress()
                    if (!response.body()?.error!!) {
                        showToastMessage("Success")
                        llVerifyOtp.visibility = View.GONE
                        llForgotPassword.visibility = View.GONE
                        llNewPassword.visibility = View.VISIBLE
                        finish()

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }


}