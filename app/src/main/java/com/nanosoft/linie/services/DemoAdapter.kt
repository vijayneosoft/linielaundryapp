package com.nanosoft.linie.services

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nanosoft.linie.R

/**
 * Created by Vijay on 8/1/20.
 */

class DemoAdapter(
    var mContext: Context,
    var mListData: ArrayList<DemoActivity.Product>,
    var mClick: click
) : RecyclerView.Adapter<DemoAdapter.WashViewHolder>() {

    var countAdd = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WashViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.adapter_inventory_items, parent, false)

        return WashViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListData.size
    }

    override fun onBindViewHolder(holder: WashViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class WashViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var txtQuantity: TextView = view.findViewById(R.id.txtQuantity)
        var imgAdd: ImageView = view.findViewById(R.id.imgAdd)

        init {
//            imgSubstract.setOnClickListener({ v -> setOnSubstract(adapterPosition) })
            imgAdd.setOnClickListener({ v -> setOnAddition(adapterPosition) })
        }

        private fun setOnAddition(adapterPosition: Int) {
            countAdd++
//            txtQuantity.setText("" + countAdd)
            mClick.onClick(mListData.get(adapterPosition), countAdd)
//            notifyItemChanged(adapterPosition)
//            notifyDataSetChanged()
        }

        fun bind(position: Int) {
            txtQuantity.setText("" + mListData.get(position).qty)

        }

    }

    interface click {
        fun onClick(get: DemoActivity.Product, countAdd: Int)
    }

}