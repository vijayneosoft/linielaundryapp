package com.nanosoft.linie.services

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nanosoft.linie.R
import com.nanosoft.linie.models.InventoryResponse
import com.nanosoft.linie.utils.Constants

/**
 * Created by Vijay on 8/1/20.
 */

class InventoryAdapter(
    var mContext: Context,
    var mInventoryResponse: InventoryResponse?,
    var mPosition: Int,
    var iTotal: ITotal,
    var mapRvSaveData: HashMap<Int, Int>,
    var mOnQuantityCallback: OnQuantityCallback,
    var mapMen: HashMap<String, String>,
    var mapWomen : HashMap<String, String>,
    var mapBoy : HashMap<String, String>,
    var mapGirl : HashMap<String, String>
) : RecyclerView.Adapter<InventoryAdapter.WashViewHolder>() {

    var listMen: List<InventoryResponse.MenDatum>? = ArrayList()
    var listWomen: List<InventoryResponse.WomenDatum>? = ArrayList()
    var listGirl: List<InventoryResponse.GirlDatum>? = ArrayList()
    var listBoy: List<InventoryResponse.BoyDatum>? = ArrayList()
    var sum: Int = 0
    var isAddCalled = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WashViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.adapter_inventory_items, parent, false)

        return WashViewHolder(view)
    }

    override fun getItemCount(): Int {

        when (mPosition) {
            0 -> {
                mInventoryResponse?.let {
                    return it.menData!!.size
                }
            }
            1 -> {
                mInventoryResponse?.let {
                    return it.womenData!!.size
                }
            }
            2 -> {
                mInventoryResponse?.let {
                    return it.boyData!!.size
                }
            }
            3 -> {
                mInventoryResponse?.let {
                    return it.girlData!!.size
                }
            }
        }
        return 0
    }

    override fun onBindViewHolder(holder: WashViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class WashViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var imgClothesType: ImageView = view.findViewById(R.id.imgClothes)
        var txtClothesName: TextView = view.findViewById(R.id.txtClothesName)
        var txtCost: TextView = view.findViewById(R.id.txtCost)
        var txtQuantity: TextView = view.findViewById(R.id.txtQuantity)
        var imgSubstract: ImageView = view.findViewById(R.id.imgSubstract)
        var imgAdd: ImageView = view.findViewById(R.id.imgAdd)
        var txtMaterialType: TextView = view.findViewById(R.id.txtMaterialType)
        var txtCalculateCost: TextView = view.findViewById(R.id.txtCalculateCost)

        var countAdd = 0

        init {
            imgClothesType = view.findViewById(R.id.imgClothes)
            txtClothesName = view.findViewById(R.id.txtClothesName)
            txtCost = view.findViewById(R.id.txtCost)
            txtQuantity = view.findViewById(R.id.txtQuantity)
            imgSubstract = view.findViewById(R.id.imgSubstract)
            imgAdd = view.findViewById(R.id.imgAdd)

            imgSubstract.setOnClickListener({ v -> setOnSubstract(adapterPosition) })
            imgAdd.setOnClickListener({ v -> setOnAddition(adapterPosition) })

        }


        private fun getList(count: Int) {
            when (mPosition) {
                0 -> {
                    listMen = mInventoryResponse?.menData
                    val calculatedAmt = count * listMen?.get(adapterPosition)?.cost!!.toInt()
                    calculateAmount(calculatedAmt, listMen?.get(adapterPosition)?.cost!!.toInt())
                    mapMen.put(listMen?.get(adapterPosition)?.id!!, count.toString())
                    mapRvSaveData.put(adapterPosition, count)
                }
                1 -> {
                    listWomen = mInventoryResponse?.womenData
                    val calculatedAmt = count * listWomen?.get(adapterPosition)?.cost!!.toInt()
                    calculateAmount(
                        calculatedAmt,
                        listWomen?.get(adapterPosition)?.cost!!.toInt()
                    )
                    mapWomen.put(listWomen?.get(adapterPosition)?.id!!, count.toString())
                    mapRvSaveData.put(adapterPosition, countAdd)

                }
                2 -> {
                    listBoy = mInventoryResponse?.boyData
                    val calculatedAmt = count * listBoy?.get(adapterPosition)?.cost!!.toInt()
                    calculateAmount(
                        calculatedAmt,
                        listBoy?.get(adapterPosition)?.cost!!.toInt()
                    )
                    mapBoy.put(listBoy?.get(adapterPosition)?.id!!, count.toString())
                    mapRvSaveData.put(adapterPosition, countAdd)
                }
                3 -> {
                    listGirl = mInventoryResponse?.girlData
                    val calculatedAmt = count * listGirl?.get(adapterPosition)?.cost!!.toInt()
                    calculateAmount(
                        calculatedAmt,
                        listGirl?.get(adapterPosition)?.cost!!.toInt()
                    )
                    mapGirl.put(listGirl?.get(adapterPosition)?.id!!, count.toString())
                    mapRvSaveData.put(adapterPosition, countAdd)
                }
            }
        }

        private fun calculateAmount(
            x: Int,
            cost: Int
        ) {

//            txtCalculateCost.setText(" = " + x)
            if (isAddCalled) {
                sum = sum + cost
            } else {
                sum = sum - cost
            }
            iTotal.cost(sum, cost, mapRvSaveData, mPosition, isAddCalled)
        }

        fun setOnSubstract(adapterPosition: Int) {
            val qty = Integer.parseInt(txtQuantity.text.toString())

            if (qty != 0) {
                countAdd = Integer.parseInt(txtQuantity.text.toString())
                countAdd--
                isAddCalled = false
                if (countAdd < 0) {
                    countAdd = 0
                    getList(countAdd)
                } else {
                    getList(countAdd)
                }
                mOnQuantityCallback.onQuantityChange(adapterPosition)
            }
        }

        private fun setOnAddition(adapterPosition: Int) {
            countAdd = Integer.parseInt(txtQuantity.text.toString())
            isAddCalled = true
            countAdd++
            getList(countAdd)
            mOnQuantityCallback.onQuantityChange(adapterPosition)
        }

        fun bind(position: Int) {

            when (mPosition) {
                0 -> {
                    try {
                        listMen = mInventoryResponse?.menData
                        txtClothesName.setText(listMen?.get(position)?.name)
                        txtCost.setText("Rs. " + listMen?.get(position)?.cost)
                        txtMaterialType.setText(listMen?.get(position)?.materailType)
                        txtQuantity.setText("" + mapRvSaveData.get(position))
                        setImageToGLide(listMen?.get(position)?.image)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                1 -> {
                    try {
                        listWomen = mInventoryResponse?.womenData
                        txtClothesName.setText(listWomen?.get(position)?.name)
                        txtCost.setText("Rs. " + listWomen?.get(position)?.cost)
                        txtMaterialType.setText(listWomen?.get(position)?.materailType)
                        txtQuantity.setText("" + mapRvSaveData.get(position))

                        setImageToGLide(listWomen?.get(position)?.image)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                2 -> {
                    try {
                        listBoy = mInventoryResponse?.boyData
                        txtClothesName.setText(listBoy?.get(position)?.name)
                        txtCost.setText("Rs. " + listBoy?.get(position)?.cost)
                        txtMaterialType.setText(listBoy?.get(position)?.materailType)
                        txtQuantity.setText("" + mapRvSaveData.get(position))

                        setImageToGLide(listBoy?.get(position)?.image)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                3 -> {
                    try {
                        listGirl = mInventoryResponse?.girlData
                        txtClothesName.setText(listGirl?.get(position)?.name)
                        txtCost.setText("Rs. " + listGirl?.get(position)?.cost)
                        txtMaterialType.setText(listGirl?.get(position)?.materailType)
                        txtQuantity.setText("" + mapRvSaveData.get(position))
                        setImageToGLide(listGirl?.get(position)?.image)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

        private fun setImageToGLide(image: String?) {
            Glide.with(mContext)
                .load(Constants.BASE_URL_INVENTORY + image)
                .into(imgClothesType)
        }
    }

    fun totalSum(): Int {
        return sum
    }

    fun getHashMapMen(): HashMap<String, String> {
        return mapMen
    }

    fun getHashMapWoMen(): HashMap<String, String> {
        return mapWomen
    }

    fun getHashMapBoy(): HashMap<String, String> {
        return mapBoy
    }

    fun getHashMapGirl(): HashMap<String, String> {
        return mapGirl
    }

    interface ITotal {
        fun cost(
            cost: Int,
            grandTotal: Int,
            mapMenSaveData: HashMap<Int, Int>,
            mPosition: Int,
            addCalled: Boolean
        )
    }

    interface OnQuantityCallback {
        fun onQuantityChange(indexPos: Int)
    }


}