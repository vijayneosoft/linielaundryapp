package com.nanosoft.linie.services

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.nanosoft.linie.R
import kotlinx.android.synthetic.main.activity_inventory.*


/**
 * Created by Vijay on 1/3/20.
 */

class DemoActivity : AppCompatActivity(), DemoAdapter.click {

    lateinit var inventoryAdapter: DemoAdapter
    var mListData = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        val a = Product()
        a.str = "AB"
        a.qty = "0"
        mListData.add(a)
        val b = Product()
        b.str = "AB"
        b.qty = "0"
        mListData.add(b)
        val c = Product()
        c.str = "AB"
        c.qty = "0"
        mListData.add(c)
        val d = Product()
        d.str = "AB"
        d.qty = "0"
        mListData.add(d)
        val e = Product()
        e.str = "AB"
        e.qty = "0"
        mListData.add(e)
        val f = Product()
        f.str = "AB"
        f.qty = "0"
        mListData.add(f)
        val g = Product()
        g.str = "AB"
        g.qty = "0"
        mListData.add(g)


        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvItems.layoutManager = linearLayoutManager

        inventoryAdapter =
            DemoAdapter(
                this,
                mListData,
                this
            )
        rvItems.adapter = inventoryAdapter

    }

    override fun onClick(
        get: Product,
        countAdd: Int
    ) {
        val i = mListData.indexOf(get)
        if (i == -1) {
            throw IndexOutOfBoundsException()
        }
        val updatedProduct = Product()
        updatedProduct.qty = get.qty +1
        updatedProduct.str = get.str

        mListData.remove(get)
        mListData.add(i, updatedProduct)
        inventoryAdapter.notifyDataSetChanged()
        Log.d("VIJAY", "onClick")
    }

    class Product{
        var str = ""
        var qty = ""
    }

}