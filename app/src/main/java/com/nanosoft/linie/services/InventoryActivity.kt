package com.nanosoft.linie.services

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.nanosoft.linie.R
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.models.InventoryResponse
import com.nanosoft.linie.utils.Navigator
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_inventory.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Vijay on 4/12/19.
 */

@SuppressLint("Registered")
class InventoryActivity : BaseActivity(), View.OnClickListener,
    InventoryAdapter.ITotal, InventoryAdapter.OnQuantityCallback {

    var mapMenSaveData = HashMap<Int, Int>()
    var mapWomenSaveData = HashMap<Int, Int>()
    var mapBoySaveData = HashMap<Int, Int>()
    var mapGirlSaveData = HashMap<Int, Int>()

    var mapMen = HashMap<String, String>()
    var mapWomen = HashMap<String, String>()
    var mapBoy = HashMap<String, String>()
    var mapGirl = HashMap<String, String>()

    var mInventoryResponse: InventoryResponse? = null
    var clothsA: ArrayList<ClothRequest> = ArrayList()
    var mOrderRequest = AddOrderRequest()
    var inventoryAdapter: InventoryAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var serviceId: String? = null
    var sumGrandTotal: Int = 0

    companion object {

        var KEY_SERVICES_ID = "keyServicesId"

        fun getCallingIntent(context: Context, serviceId: String?): Intent {
            val intent = Intent(context, InventoryActivity::class.java)
            intent.putExtra(KEY_SERVICES_ID, serviceId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory)

        val bundle = intent.extras
        if (bundle != null) {
            serviceId = bundle.getString(KEY_SERVICES_ID)
        }

        initListeners()
        initToolbar()
        showProgress(this)
        callInventoryListingApi(serviceId)
        rdMan.isChecked = true
    }


    private fun initToolbar() {
        setToolbarWithTitle("Inventory")
        showBackArrow()
    }

    private fun callInventoryListingApi(serviceId: String?) {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        mCustomerService?.inventoryListingApi(token!!, serviceId!!)
            ?.enqueue(object : Callback<InventoryResponse> {
                override fun onResponse(
                    call: Call<InventoryResponse>,
                    response: Response<InventoryResponse>
                ) {

                    if (!response.body()?.error!!) {
                        hideProgress()
                        mInventoryResponse = response.body()

                        if (mapMenSaveData.size <= 0) {
                            for (i in 0..mInventoryResponse?.menData?.size!! - 1) {
                                mapMenSaveData.put(i, 0)
                            }
                        }
                        setListAdapter(0, mapMenSaveData)
                    } else {
                        showToastMessage("" + response.body()!!.title)
                        hideProgress()
                    }
                }

                override fun onFailure(call: Call<InventoryResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }


    private fun setListAdapter(
        mPosition: Int,
        mapMenSaveData: HashMap<Int, Int>
    ) {
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvItems.layoutManager = linearLayoutManager

        inventoryAdapter =
            InventoryAdapter(
                this,
                mInventoryResponse,
                mPosition,
                this,
                mapMenSaveData,
                this,
                mapMen,
                mapWomen,
                mapBoy,
                mapGirl
            )
        rvItems.adapter = inventoryAdapter

    }

    private fun initListeners() {
        rdMan.setOnClickListener(this)
        rdWoman.setOnClickListener(this)
        rdBoys.setOnClickListener(this)
        rdGirls.setOnClickListener(this)
        btnNext_wash.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.rdMan -> {
                if (mapMenSaveData.size <= 0) {
                    for (i in 0..mInventoryResponse?.menData?.size!! - 1) {
                        mapMenSaveData.put(i, 0)
                    }
                }
                setListAdapter(0, mapMenSaveData)
            }
            R.id.rdWoman -> {
                if (mapWomenSaveData.size <= 0) {
                    for (i in 0..mInventoryResponse?.womenData?.size!! - 1) {
                        mapWomenSaveData.put(i, 0)
                    }
                }
                setListAdapter(1, mapWomenSaveData)
            }
            R.id.rdBoys -> {
                if (mapBoySaveData.size <= 0) {
                    for (i in 0..mInventoryResponse?.boyData?.size!! - 1) {
                        mapBoySaveData.put(i, 0)
                    }
                }
                setListAdapter(2, mapBoySaveData)
            }
            R.id.rdGirls -> {
                if (mapGirlSaveData.size <= 0) {
                    for (i in 0..mInventoryResponse?.girlData?.size!! - 1) {
                        mapGirlSaveData.put(i, 0)
                    }
                }
                setListAdapter(3, mapGirlSaveData)
            }
            R.id.btnNext_wash -> {

                mOrderRequest.name = ""
                mOrderRequest.discount = 0
                mOrderRequest.payment_mode = "COD"
                mOrderRequest.pickup_date = ""
                mOrderRequest.user_id = ""
                mOrderRequest.service_id = serviceId!!
                mOrderRequest.status = "Requested"
                mOrderRequest.user_type = "buyer"

                Log.d("order", "" + inventoryAdapter?.totalSum())
                val mapMen = inventoryAdapter?.mapMen
                val mapWomen = inventoryAdapter?.mapWomen
                val mapBoy = inventoryAdapter?.mapBoy
                val mapGirl = inventoryAdapter?.mapGirl

                setMap(mapMen!!)
                setMap(mapWomen!!)
                setMap(mapBoy!!)
                setMap(mapGirl!!)
                mOrderRequest.cloths = clothsA
                mOrderRequest.final_cost = sumGrandTotal

                if (sumGrandTotal != 0) {
                    val mOrderData = gson.toJson(mOrderRequest)
                    Navigator.navigateToOrderConfirmActivity(this, mOrderData)
                }
            }
        }
    }

    private fun setMap(mapMen: Map<String, String>) {

        val entrySet = mapMen.entries

        entrySet.forEach {
            if (!it.value.equals("0", true)) {
                val cloths = ClothRequest()
                cloths.quantity = it.value.toString()
                cloths.selectedCloth = it.key.toString()
                clothsA.add(cloths)
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun cost(
        cost: Int,
        grandTotal: Int,
        mapMenSaveData: HashMap<Int, Int>,
        mPosition: Int,
        addCalled: Boolean
    ) {
        if (addCalled) {
            sumGrandTotal = sumGrandTotal + grandTotal
        } else {
            if (sumGrandTotal <= 0) {
                sumGrandTotal = 0
            } else {
                sumGrandTotal = sumGrandTotal - grandTotal
            }
        }
        totalCostWash.setText("Rs " + sumGrandTotal)

        when (mPosition) {
            0 -> {
                mapMenSaveData.entries.forEach {
                    this.mapMenSaveData.put(it.key, it.value)
                }
            }
            1 -> {
                mapMenSaveData.entries.forEach {
                    this.mapWomenSaveData.put(it.key, it.value)
                }
            }
            2 -> {
                mapMenSaveData.entries.forEach {
                    this.mapBoySaveData.put(it.key, it.value)
                }
            }
            3 -> {
                mapMenSaveData.entries.forEach {
                    this.mapGirlSaveData.put(it.key, it.value)
                }
            }
        }
    }

    override fun onQuantityChange(indexPos: Int) {
        inventoryAdapter?.notifyItemChanged(indexPos)
        inventoryAdapter?.notifyDataSetChanged()

    }
}
