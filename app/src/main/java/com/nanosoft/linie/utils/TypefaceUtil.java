package com.nanosoft.linie.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.CharacterStyle;

import com.nanosoft.linie.UserApplication;


public class TypefaceUtil {
    public final static int PROXIMA_NOVA_REGULAR = 0;
    public final static int PROXIMA_NOVA_REGULAR_ITALIC = 1;
    public final static int PROXIMA_NOVA_SEMIBOLD = 2;
    public final static int PROXIMA_NOVA_SEMIBOLD_ITALIC = 3;
    public final static int PROXIMA_NOVA_LIGHT = 4;
    public final static int PROXIMA_NOVA_LIGHT_ITALIC = 5;
    public final static int PROXIMA_NOVA_BOLD = 6;
    public final static int PROXIMA_NOVA_EXTRA_BOLD = 7;
    public final static int PROXIMA_NOVA_BLACK = 8;
    private static TypefaceUtil ourInstance = new TypefaceUtil();
    private static Typeface tfPnRegular;
    private static Typeface tfPnRegularItalic;
    private static Typeface tfPnSemibold;
    private static Typeface tfPnSemiboldItalic;
    private static Typeface tfPnLight;
    private static Typeface tfPnLightItalic;
    private static Typeface tfPnBold;
    private static Typeface tfPnExtraBold;
    private static Typeface tfPnBlack;

    private TypefaceUtil() {
        //instantiate all typefaces
        Context context = UserApplication.Companion.getMAppContext();
        if (context == null) {
            return;
        }

        AssetManager assMan = context.getAssets();
        tfPnRegular = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Regular.otf");
        tfPnRegularItalic = Typeface.createFromAsset(assMan, "fonts/ProximaNova-RegularItalic.otf");
        tfPnSemibold = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Semibold.otf");
        tfPnSemiboldItalic = Typeface.createFromAsset(assMan, "fonts/ProximaNova-SemiboldItalic.otf");
        tfPnLight = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Light.otf");
        tfPnLightItalic = Typeface.createFromAsset(assMan, "fonts/ProximaNova-LightItalic.otf");
        tfPnBold = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Bold.otf");
        tfPnExtraBold = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Extrabold.otf");
        tfPnBlack = Typeface.createFromAsset(assMan, "fonts/ProximaNova-Black.otf");
    }

    public static TypefaceUtil getInstance() {
        return ourInstance;
    }

    public static Typeface getTypeface(int typefaceEnum) {
        switch (typefaceEnum) {
            case PROXIMA_NOVA_REGULAR:
                return tfPnRegular;
            case PROXIMA_NOVA_REGULAR_ITALIC:
                return tfPnRegularItalic;
            case PROXIMA_NOVA_SEMIBOLD:
                return tfPnSemibold;
            case PROXIMA_NOVA_SEMIBOLD_ITALIC:
                return tfPnSemiboldItalic;
            case PROXIMA_NOVA_LIGHT:
                return tfPnLight;
            case PROXIMA_NOVA_LIGHT_ITALIC:
                return tfPnLightItalic;
            case PROXIMA_NOVA_BOLD:
                return tfPnBold;
            case PROXIMA_NOVA_EXTRA_BOLD:
                return tfPnExtraBold;
            case PROXIMA_NOVA_BLACK:
                return tfPnBlack;
            default:
                return tfPnLight;
        }
    }

    public static boolean isItalicTypeface(Typeface tf) {
        return (tf == getTypeface(TypefaceUtil.PROXIMA_NOVA_REGULAR_ITALIC) ||
                tf == getTypeface(TypefaceUtil.PROXIMA_NOVA_SEMIBOLD_ITALIC) ||
                tf == getTypeface(TypefaceUtil.PROXIMA_NOVA_LIGHT_ITALIC));
    }

    public static SpannableString getTypeFacedSpannableString(Context context, CharSequence string) {
        return TypefaceUtil.getTypeFacedSpannableString(context,
                string,
                TypefaceUtil.PROXIMA_NOVA_REGULAR);
    }

    public static SpannableString getTypeFacedSpannableString(Context context, CharSequence string, int typefaceEnum) {
        if (string == null) string = "";
        SpannableString s = new SpannableString(string);
        s.setSpan(new TypefaceSpan(typefaceEnum), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return s;
    }


    public static SpannableString setStyles(CharSequence text, CharacterStyle... styles) {
        return setStyles(text, 0, text.length(), styles);
    }

    public static SpannableString setStyles(CharSequence text, int start, int end, CharacterStyle... styles) {
        SpannableString ss = new SpannableString(text);
        for (Object span : styles) {
            ss.setSpan(span, start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        return ss;
    }
}
