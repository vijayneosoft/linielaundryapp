package com.nanosoft.linie.utils;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

import androidx.collection.LruCache;

/**
 * Style a {@link Spannable} with a custom {@link Typeface}.
 *
 * @author Tristan Waddington
 */
public class TypefaceSpan extends MetricAffectingSpan {
    /** An <code>LruCache</code> for previously loaded typefaces. */
    private static LruCache<Integer, Typeface> sTypefaceCache = new LruCache<>(12);

    private Typeface mTypeface;

    /**
     * Load the {@link Typeface} and apply to a {@link Spannable}.
     */
    public TypefaceSpan(int typefaceEnum) {
        //try cache
        mTypeface = sTypefaceCache.get(typefaceEnum);

        if (mTypeface == null) {
            mTypeface = TypefaceUtil.getTypeface(typefaceEnum);
        }

        if (mTypeface == null) {
//            mTypeface = Typeface.createFromAsset(context.getApplicationContext()
//                    .getAssets(), String.format("fonts/%s.otf", typefaceName));
            mTypeface = TypefaceUtil.getTypeface(TypefaceUtil.PROXIMA_NOVA_LIGHT);
        }

        if (mTypeface != null) {
            // Cache the loaded Typeface
            sTypefaceCache.put(typefaceEnum, mTypeface);
        }
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTypeface(mTypeface);

        // Note: This flag is required for proper typeface rendering
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(mTypeface);

        // Note: This flag is required for proper typeface rendering
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}