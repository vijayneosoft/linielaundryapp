package com.nanosoft.linie.utils

import android.content.Context
import android.content.SharedPreferences
import com.nanosoft.linie.R
import com.nanosoft.linie.UserApplication

/**
 * Created by Vijay on 3/12/19.
 */

class SharedPreferenceManager {

    companion object{
        var KEY_USER_REGISTERED = "keyUserRegistered"
        var KEY_TOKEN = "keyToken"
        var KEY_NAME = "keyName"
        var KEY_USER_ID = "keyUserId"
        var KEY_DEVICE_TOKEN = "deviceToken"

        var KEY_DEVICE_MOBILE = "mobile"
        var KEY_DEVICE_EMAIL = "email"
        var KEY_DEVICE_GENDER = "gender"
        var KEY_DEVICE_CITY = "city"
        var KEY_DEVICE_DOB = "dob"


    }

    var sharedPreferenceManger: SharedPreferenceManager? = null
    var sharedPreferences: SharedPreferences? = null

    init {
        sharedPreferences = UserApplication.getAppInstance()
            .getSharedPreferences(
                UserApplication.getAppInstance().getString(R.string.preference_name),
                Context.MODE_PRIVATE
            )
    }

    fun saveString(key: String, value: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value)
        editor?.commit()
        editor?.apply()
    }

    fun containsKey(key: String): Boolean {
        return sharedPreferences?.contains(key)!!
    }

    fun getString(key: String): String {
        return sharedPreferences?.getString(key, "")!!
    }

    fun saveInt(key: String, value: Int) {
        sharedPreferences?.edit()?.putInt(key, value)?.apply()
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences?.edit()?.putBoolean(key, value)?.apply()
    }

    fun getBoolean(key: String): Boolean? {
        return sharedPreferences?.getBoolean(key, false)
    }

    fun getInt(key: String): Int {
        return sharedPreferences?.getInt(key, 0)!!
    }

    fun clearData() {
        val editor = sharedPreferences?.edit()
        editor?.clear()
        editor?.commit()
    }

}