package com.nanosoft.linie.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.nanosoft.linie.OpenPdfActivity
import com.nanosoft.linie.SplashActivity
import com.nanosoft.linie.address.OrderConfirmActivity
import com.nanosoft.linie.dashboard.DashboardActivity
import com.nanosoft.linie.dashboard.OrdersDetailsActivity
import com.nanosoft.linie.login.ForgotPasswordActivity
import com.nanosoft.linie.login.signIn.LoginActivity
import com.nanosoft.linie.login.signup.SignUpActivity
import com.nanosoft.linie.services.InventoryActivity

/**
 * Created by Vijay on 3/12/19.
 */

class Navigator {

    companion object {

        fun navigateToLoginActivity(context: Activity) {
            return context.startActivity(Intent(context, LoginActivity::class.java))
        }

        fun navigateToOpenActivity(context: Activity,value : Boolean) {
            return context.startActivity(
                OpenPdfActivity.getCallingIntent(
                    context,value
                )
            )
        }

        fun navigateToDashboardActivity(context: Activity) {
            return context.startActivity(
                DashboardActivity.getCallingIntent(
                    context
                )
            )
        }

        fun navigateToForgotActivity(context: Activity, fromScreen: Boolean) {
            return context.startActivity(
                ForgotPasswordActivity.getCallingIntent(
                    context,
                    fromScreen
                )
            )
        }

        fun navigateToWashActivity(
            context: Context,
            data: String?
        ) {
            return context.startActivity(
                InventoryActivity.getCallingIntent(
                    context,
                    data
                )
            )
        }

        fun navigateToSignUpActivity(context: Context, fromScreen: Boolean) {
            return context.startActivity(
                SignUpActivity.getCallingIntent(
                    context,
                    fromScreen
                )
            )
        }

        fun navigateToSplashActivity(context: Activity) {
            return context.startActivity(Intent(context, SplashActivity::class.java))
        }

        fun navigateToOrderConfirmActivity(context: Activity, mOrderData: String) {
            return context.startActivity(
                OrderConfirmActivity.getCallingIntent(
                    context,mOrderData
                )
            )
        }

        fun navigateToOrdersDetailsActivity(context: Activity, data: String) {
            return context.startActivity(
                OrdersDetailsActivity.getCallingIntent(
                    context, data
                )
            )
        }


    }


}