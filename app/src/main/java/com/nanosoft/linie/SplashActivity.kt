package com.nanosoft.linie

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.AnimationUtils
import com.nanosoft.linie.base.BaseActivity
import com.nanosoft.linie.utils.Navigator
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_splash.*


/**
 * Created by Vijay on 4/12/19.
 */

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val anim = AnimationUtils.loadAnimation(this, R.anim.scale);
        view.startAnimation(anim)

        val isUserRegistered =
            mSharedPreferenceManager?.getBoolean(SharedPreferenceManager.KEY_USER_REGISTERED)
        Log.d("splash", "" + isUserRegistered)

        Handler().postDelayed({
            if (isUserRegistered!!) {
                Navigator.navigateToDashboardActivity(this)
                finish()
            } else {
                Navigator.navigateToLoginActivity(this)
                finish()
            }
        }, 2000)

    }

}