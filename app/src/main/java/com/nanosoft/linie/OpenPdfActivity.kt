package com.nanosoft.linie

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.Bundle
import android.os.ParcelFileDescriptor
import com.nanosoft.linie.base.BaseActivity
import kotlinx.android.synthetic.main.activity_open_pdf.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


/**
 * Created by Vijay on 2/3/20.
 */

class OpenPdfActivity : BaseActivity() {

    private lateinit var fileDescriptor: ParcelFileDescriptor
    private lateinit var pdfRenderer: PdfRenderer
    private lateinit var currentPage: PdfRenderer.Page


    companion object {

        fun getCallingIntent(context: Context, value: Boolean): Intent {
            val intent = Intent(context, OpenPdfActivity::class.java)
            intent.putExtra("fromFaq", value)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_pdf)
        val bundle = intent.extras
        if (bundle != null) {
            val data = bundle.getBoolean("fromFaq", false)
            if (data) {
                openRenderer(this, "faq.pdf")
                showPage(0)
            } else {
                openRenderer(this, "terms.pdf")
                showPage(0)
            }
        }
    }

    @Throws(IOException::class)
    private fun openRenderer(context: Context?, filename: String) {
        if (context == null) return

        // In this sample, we read a PDF from the assets directory.
        val file = File(context.cacheDir, filename)
        if (!file.exists()) {
            // Since PdfRenderer cannot handle the compressed asset file directly, we copy it into
            // the cache directory.
            val asset = context.assets.open(filename)
            val output = FileOutputStream(file)
            val buffer = ByteArray(1024)
            var size = asset.read(buffer)
            while (size != -1) {
                output.write(buffer, 0, size)
                size = asset.read(buffer)
            }
            asset.close()
            output.close()
        }
        fileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY)
        // This is the PdfRenderer we use to render the PDF.
        pdfRenderer = PdfRenderer(fileDescriptor)
        currentPage = pdfRenderer.openPage(0)
    }

    @Throws(IOException::class)
    private fun closeRenderer() {
        currentPage.close()
        pdfRenderer.close()
        fileDescriptor.close()
    }

    private fun showPage(index: Int) {
        if (pdfRenderer.pageCount <= index) return

        // Make sure to close the current page before opening another one.
        currentPage.close()
        // Use `openPage` to open a specific page in PDF.
        currentPage = pdfRenderer.openPage(index)
        // Important: the destination bitmap must be ARGB (not RGB).
        val bitmap =
            Bitmap.createBitmap(currentPage.width, currentPage.height, Bitmap.Config.ARGB_8888)
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
        currentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
        // We are ready to show the Bitmap to user.
        image.setImageBitmap(bitmap)

    }
}