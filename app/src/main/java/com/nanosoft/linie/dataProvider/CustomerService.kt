package com.nanosoft.linie.dataProvider

import com.nanosoft.linie.models.AddAddressRequest
import com.nanosoft.linie.dashboard.GetOrderDetailsReponse
import com.nanosoft.linie.models.*
import com.nanosoft.linie.services.AddOrderRequest
import com.nanosoft.linie.models.InventoryResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Vijay on 3/12/19.
 */

interface CustomerService {

    @POST("users/login")
    @FormUrlEncoded
    fun doLogin(
        @Field("device_token") deviceToken: ArrayList<String>,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("user_type") userType: String
    ): Call<LoginResponse>


    @POST("users/signup")
    @FormUrlEncoded
    fun doSignUp(
        @Field("name") name: String,
        @Field("user_type") userType: String,
        @Field("email") email: String,
        @Field("gender") gender: String,
        @Field("dob") dob: String,
        @Field("mobile") mobile: String,
        @Field("user_city") userCity: String,
        @Field("user_state") userState: String,
        @Field("password") password: String
    ): Call<LoginResponse>


    @POST("users/forgotPassword")
    @FormUrlEncoded
    fun forgotPassword(
        @Header("token") token: String,
        @Field("email") email: String
    ): Call<ForgotPasswordResponse>

    @POST("users/verifyOTP")
    @FormUrlEncoded
    fun verifyOtpApi(
        @Field("token") token: String
    ): Call<ForgotPasswordResponse>


    //from --> changepassword

    @POST("users/changePassword")
    @FormUrlEncoded
    fun changePasswordApi(
        @Header("token") token: String,
        @Field("oldPassword") oldPassword: String,
        @Field("newPassword") newPassword: String,
        @Field("from") fromScreenName: String

    ): Call<ForgotPasswordResponse>


    //from --> forgotpassword

    @POST("users/changePassword")
    @FormUrlEncoded
    fun newPasswordApi(
        @Header("token") token: String,
        @Field("newPassword") newPassword: String,
        @Field("from") fromScreenName: String
    ): Call<ForgotPasswordResponse>

    @POST("services/servicesListing")
    fun servicesListingApi(
        @Header("token") token: String
    ): Call<ServicesResponse>

    @POST("inventory/inventoryListing")
    @FormUrlEncoded
    fun inventoryListingApi(
        @Header("token") token: String,
        @Field("service_id") serviceId: String
    ): Call<InventoryResponse>

    @POST("orders/addOrder")
    fun addOrderApi(
        @Header("token") token: String,
        @Body order: AddOrderRequest
    ): Call<AddOrderResponse>

    @POST("users/editUser")
    @FormUrlEncoded
    fun editUserApi(
        @Header("token") token: String,
        @Field("profileImage") profileImage: String,
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("dob") dob: String,
        @Field("gender") gender: String,
        @Field("user_city") userCity: String,
        @Field("user_address") userAddress: String,
        @Field("user_state") userState: String,
        @Field("user_country") userCountry: String
    ): Call<EditResponse>


    @POST("users/addDeliveryAddress")
    fun addDeliveryAddressApi(
        @Header("token") token: String,
        @Body addressRequest: AddAddressRequest
    ): Call<AddAddressResponse>

    @POST("users/getDeliveryAddressData")
    @FormUrlEncoded
    fun getDeliveryAddressDataApi(
        @Header("token") token: String,
        @Field("user_id") user_id: String

    ): Call<GetAddressResponse>

    @POST("orders/getUserOrdersListing")
    @FormUrlEncoded
    fun getUserOrdersListingApi(
        @Header("token") token: String,
        @Field("user_id") userId: String
    ): Call<GetOrderDetailsReponse>

}