package com.nanosoft.linie.base

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.nanosoft.linie.R
import com.nanosoft.linie.dataProvider.ApiService
import com.nanosoft.linie.dataProvider.CustomerService
import com.nanosoft.linie.models.ForgotPasswordResponse
import com.nanosoft.linie.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.toolbar_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Vijay on 3/12/19.
 */
@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    var mCustomerService: CustomerService? = null
    var progressDialog: ProgressDialog? = null
    var mSharedPreferenceManager: SharedPreferenceManager? = null
    var gson = Gson()

    init {
        mSharedPreferenceManager = SharedPreferenceManager()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        mCustomerService = ApiService.getInstance().call()

        if (!isNetConnected(this)) {
            showToastMessage("Internet is off ! Please Turn on")
        }

    }


    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        container?.addView(LayoutInflater.from(this).inflate(layoutResID, null))

    }

    fun replaceFragment(@IdRes containerViewId: Int, @NonNull fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            .replace(containerViewId, fragment)
            .commit()
    }

    fun isNetConnected(context: Context): Boolean {
        val mConnectivityManager = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val netInfo = mConnectivityManager.activeNetworkInfo
        return if (netInfo != null && netInfo.isConnectedOrConnecting) {
            true
        } else false
    }

    fun showToastMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


    fun showProgress(context: Context) {

        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(context, null, null, true, false)
            progressDialog!!.setContentView(R.layout.custom_dialog)
            progressDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            //progressDialog.setProgressStyle(R.style.popupStyle);
            progressDialog!!.setCancelable(true)
        }
    }

    fun hideProgress() {
        if (progressDialog != null && progressDialog?.isShowing()!!) {
            progressDialog?.dismiss()
            progressDialog = null
        }
        // LoadingDialog.dismissLast();
    }

    fun openNewActivity(
        context: Context,
        activityToOpen: Class<out Activity>
    ) {
        var intent = Intent(context, activityToOpen)
        startActivity(intent)
    }

    private fun callForgotPasswordApi(edtEmailForgot: String) {
        val token = mSharedPreferenceManager?.getString(SharedPreferenceManager.KEY_TOKEN)
        mCustomerService?.forgotPassword(token!!, edtEmailForgot)
            ?.enqueue(object : Callback<ForgotPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgotPasswordResponse>,
                    response: Response<ForgotPasswordResponse>
                ) {

                    if (!response.body()?.error!!) {
                        hideProgress()
                        showToastMessage("Success")

                    } else {
                        showToastMessage("" + response.body()!!.title)
                    }
                }

                override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                    showToastMessage(getString(R.string.error_something_went_wrong))
                    hideProgress()

                }
            })
    }

    fun setToolbarWithTitle(title: String) {

        if (toolbar != null) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowTitleEnabled(false)

            val mToolBarTitle = toolbar.findViewById<TextView>(R.id.txtTitle_home_tl)
            mToolBarTitle.setText(title)
        }
    }


    fun showBackArrow() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    fun hideBackArrow() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }


}